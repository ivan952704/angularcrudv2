import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { HomeComponent } from './home.component';

import { EmployeeListResolverService } from './employees/employee-list-resolver.service';
import { EmployeeService } from './employees/employee.service';
import { EmployeeDetailsGuardService } from './employees/employee-details-guard.service';
import { CreateEmployeeCanDeactivateGuardService } from './employees/create-employee-can-deactivate-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    EmployeeListResolverService,
    EmployeeDetailsGuardService,
    EmployeeService,
    CreateEmployeeCanDeactivateGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
