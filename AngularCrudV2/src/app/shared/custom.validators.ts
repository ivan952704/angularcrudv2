import { AbstractControl } from '@angular/forms';

export class CustomValidators {

    static defaultSelected(defaultValue: string) {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const selectedValue: string = control.value;
            if (selectedValue !== defaultValue) {
                return null;
            }
            return { defaultSelected: true };
        };
    }

    static emailDomain(domainName: string) {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const email: string = control.value;
            if (email != null) {
                const domain = email.substring(email.lastIndexOf('@') + 1);
                if (email === '' || domain.toLowerCase() === domainName.toLowerCase()) {
                    return null;
                }
                return { emailDomain: true };
            }
            return null;
        };
    }

    static matchEmail(group: AbstractControl): { [key: string]: any } | null {
        const emailControl = group.get('email');
        const confirmEmailControl = group.get('confirmEmail');

        if (emailControl.value === confirmEmailControl.value ||
            (confirmEmailControl.pristine && confirmEmailControl.value === '')) {
            return null;
        }

        return { emailMismatch: true };
    }
}
