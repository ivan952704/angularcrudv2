import { NgModule } from '@angular/core';
import { EmployeeRoutingModule } from './employee-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ListEmployeesComponent } from './list-employees.component';
import { DisplayEmployeeComponent } from './display-employee.component';
import { EmployeeDetailsComponent } from './employee-details.component';
import { CreateEmployeeComponent } from './create-employee.component';

@NgModule({
  declarations: [
    ListEmployeesComponent,
    DisplayEmployeeComponent,
    EmployeeDetailsComponent,
    CreateEmployeeComponent
  ],
  imports: [
    EmployeeRoutingModule,
    SharedModule
  ]
})
export class EmployeeModule { }
