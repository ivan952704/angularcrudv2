import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListEmployeesComponent } from './list-employees.component';
import { EmployeeDetailsComponent } from './employee-details.component';
import { CreateEmployeeComponent } from './create-employee.component';

import { EmployeeListResolverService } from './employee-list-resolver.service';
import { EmployeeDetailsGuardService } from './employee-details-guard.service';
import { CreateEmployeeCanDeactivateGuardService } from './create-employee-can-deactivate-guard.service';

const employeeRoutes: Routes = [
  {
    path: '',
    component: ListEmployeesComponent,
    resolve: { employeeList: EmployeeListResolverService }
  },
  {
    path: 'create',
    component: CreateEmployeeComponent,
    canDeactivate: [CreateEmployeeCanDeactivateGuardService]
  },
  {
    path: 'edit/:id',
    component: CreateEmployeeComponent,
    canDeactivate: [CreateEmployeeCanDeactivateGuardService]
  },
  {
    path: ':id',
    component: EmployeeDetailsComponent,
    canActivate: [EmployeeDetailsGuardService]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(employeeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeeRoutingModule { }
