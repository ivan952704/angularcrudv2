import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { EmployeeService } from './employee.service';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Employee } from '../models/employee.model';

@Injectable()
export class EmployeeListResolverService implements Resolve<Employee[] | string> {

    constructor(private _employeeService: EmployeeService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Employee[] | string> {
        // No hacemos un .subscribe porque el resolver automaticamente subscribe el servicio
        // si llamas el servicio en otro componente se tiene que subscribir
        // hacemos un resolver para que la pagina no aparezca vacia y luego salgan los registros
        // primero se espera que el resolver obtenga los datos y luego renderiza la vista
        // es decir te quedaras viendo el loading de la pantalla anterior hasta que se obtengan los datos

        return this._employeeService.getEmployees()
            .pipe(catchError((error: string) => of(error)));
    }
}
