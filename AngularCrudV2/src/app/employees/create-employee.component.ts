import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Employee } from '../models/employee.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from './employee.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/public_api';
import { Department } from '../models/department.model';
import { CustomValidators } from '../shared/custom.validators';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  employee: Employee;
  employeeForm: FormGroup;

  departments: Department[] = [
    { id: 1, name: 'Help Desk' },
    { id: 2, name: 'HR' },
    { id: 3, name: 'IT' },
    { id: 4, name: 'Payroll' }
  ];

  previewPhoto = false;
  panelTitle = '';

  submitted = false;
  isSaving = false;
  formErrors = {};

  validationMessages = {
    fullName: {
      required: 'Full Name is required.',
      minlength: 'Full Name must be greater than 2 characters.',
      maxlength: 'Full Name must be less than 10 characters.'
    },
    gender: {
      required: 'Gender is required.'
    },
    dateOfBirth : {
      required: 'Date of Birth is required.'
    },
    contactPreference: {
      required: 'Contact Preference is required.'
    },
    department: {
      defaultSelected: 'Department is required.'
    },
    email: {
      required: 'Email is required.',
      emailDomain: 'Email domain should be gmail.com'
    },
    confirmEmail: {
      required: 'Confirm Email is required.'
    },
    phone: {
      required: 'Phone is required.'
    },
    photoPath: {
      required: 'Photo Path is required.'
    },
    skillName: {
      required: 'Skill Name is required.',
    },
    experienceInYears: {
      required: 'Experience is required.',
      max: 'Experience should be less than 11'
    },
    proficiency: {
      required: 'Proficiency is required.',
    },
    emailGroup: {
      emailMismatch: 'Email and Confirm Email do not match.'
    }
  };

  datePickerConfig: Partial<BsDatepickerConfig>;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private employeeService: EmployeeService) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        minDate: new Date(1990, 0, 1),
        maxDate: new Date(2019, 11, 31)
      }
    );
  }

  ngOnInit() {
    this.initializeEmployee();
    this.initializeEmployeeForm();

    this.employeeForm.valueChanges.subscribe(
      () => this.logValidationErrors(this.employeeForm)
    );

    this.employeeForm.get('contactPreference').valueChanges.subscribe(
      (value: string) => this.onContactPreferenceChange(value)
    );

    this.route.paramMap.subscribe(params => {
      const id: number = +params.get('id');
      if (id) {
        this.panelTitle = 'Edit Employee';
        this.getEmployee(id);
      } else {
        this.panelTitle = 'Create Employee';
      }
    });
  }

  private initializeEmployee() {
    this.employee = {
      id: null,
      name: '',
      gender: 'Male',
      contactPreference: 'email',
      phoneNumber: null,
      email: '',
      dateOfBirth: new Date(1995, 0, 1),
      department: '',
      isActive: true,
      photoPath: '',
      password: null,
      confirmPassword: null
    };
  }

  private initializeEmployeeForm() {
    this.employeeForm = this.formBuilder.group({
      fullName: [this.employee.name, [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      gender: [this.employee.gender, [Validators.required]],
      dateOfBirth: [this.employee.dateOfBirth, [Validators.required]],
      department: [this.employee.department, [CustomValidators.defaultSelected('')]],
      isActive: [this.employee.isActive],
      contactPreference: [this.employee.contactPreference, [Validators.required]],
      email: [this.employee.email, [Validators.required, CustomValidators.emailDomain('gmail.com')]],
      phone: [this.employee.phoneNumber],
      photoPath: [this.employee.photoPath, [Validators.required]]
    });
  }

  private setEmployeeForm() {
    this.employeeForm.patchValue({
      fullName: this.employee.name,
      gender: this.employee.gender,
      dateOfBirth: this.employee.dateOfBirth,
      department: this.employee.department,
      isActive: this.employee.isActive,
      contactPreference: this.employee.contactPreference,
      email: this.employee.email,
      phone: this.employee.phoneNumber,
      photoPath: this.employee.photoPath
    });
  }

  private getEmployee(id: number) {
    this.employeeService.getEmployee(id).subscribe(
      (employee) => {
        employee.dateOfBirth = new Date(employee.dateOfBirth);
        this.employee = employee;
        this.setEmployeeForm();
      },
      (error) => console.log(error)
    );
  }

  onContactPreferenceChange(selectedValue: string) {
    const phoneControl = this.employeeForm.get('phone');
    const emailControl = this.employeeForm.get('email');

    if (selectedValue === 'phone') {
      phoneControl.setValidators(Validators.required);
      emailControl.clearValidators();
    } else {
      phoneControl.clearValidators();
      emailControl.setValidators(Validators.required);
    }

    phoneControl.updateValueAndValidity();
    emailControl.updateValueAndValidity();
  }

  togglePhotoPreview(): void {
    this.previewPhoto = !this.previewPhoto;
  }

  private setEmployee() {
    this.employee.name = this.employeeForm.value.fullName;
    this.employee.gender = this.employeeForm.get('gender').value;
    this.employee.isActive = this.employeeForm.get('isActive').value;
    this.employee.dateOfBirth = this.employeeForm.get('dateOfBirth').value;
    this.employee.department = this.employeeForm.get('department').value;
    this.employee.contactPreference = this.employeeForm.get('contactPreference').value;
    this.employee.email = this.employeeForm.get('email').value;
    this.employee.phoneNumber = this.employeeForm.get('phone').value;
    this.employee.photoPath = this.employeeForm.get('photoPath').value;
  }

  onSubmit() {
    this.submitted = true;
    this.logValidationErrors(this.employeeForm);
    if (this.employeeForm.invalid) {
      return;
    }
    this.isSaving = true;

    this.setEmployee();

    if (this.employee.id == null) {
      this.employeeService.createEmployee(this.employee).subscribe(
        (data: Employee) => {
          console.log(data);
          this.employeeForm.reset();
          this.router.navigate(['employees']);
        },
        (error) => {
          console.log(error);
          this.isSaving = false;
        }
      );
    } else {
      this.employeeService.updateEmployee(this.employee).subscribe(
        () => {
          this.employeeForm.reset();
          this.router.navigate(['employees']);
        },
        (error) => {
          console.log(error);
          this.isSaving = false;
        }
      );
    }
  }

  onCancelClick() {
    this.router.navigate(['employees']);
  }

  logValidationErrors(group: FormGroup = this.employeeForm) {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);

      this.formErrors[key] = '';
      if (abstractControl && !abstractControl.valid &&
        (abstractControl.touched || abstractControl.dirty || abstractControl.value || this.submitted)) {
        const messages = this.validationMessages[key];
        for (const errorKey in abstractControl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + ' ';
          }
        }
      }

      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      }
    });
  }
}
